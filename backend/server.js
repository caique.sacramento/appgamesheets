const express = require('express')
const bodyParser = require('body-parser')
const app = express()
const PORT = 3001


app.use (express.json())
app.use (bodyParser.urlencoded({extended:true}))
app.use (bodyParser.json())

app.use('/', require('./routes/oi'))
app.use('/users', require('./routes/api/users'))
app.use('/jogos', require('./routes/api/jogos'))




app.listen(PORT, () => {console.log(`App rodando na porta ${PORT}`)})