const express = require('express')
let { usersList, User } = require ('../../models/users')
const router = express.Router()


router.get('/:userId', (req, res)=> {
    try {
        let users = usersList.filter(u => u.id == req.params["userId"])
        if (users.length > 0){
            res.send(users[0])
        } else {
            res.status(404).send({"error": "Usuário não existe!"})
        }
    } catch(err) {
        console.error(err.message)
        res.status(500).send({"error" : "Server Error"})
    }
})

router.delete('/:userId', (req, res)=> {
    try {
        let users = usersList.filter(u => u.id == req.params["userId"])
        if (users.length > 0){
            usersList = usersList.filter(u => u.id != req.params["userId"])
            res.send(usersList)
        } else {
            res.status(404).send({"error": "Usuário não existe!"})
        }
    } catch(err) {
        console.error(err.message)
        res.status(500).send({"error" : "Server Error"})
    }
})
router.put('/:userId', (req, res)=> {
    try {
        let users = usersList.filter(u => u.id == req.params["userId"])
        if (users.length > 0){
            users = users[0]
            let { name, email, password, is_active, is_admin } = req.body
            users.name = name
            users.email = email
            users.password = password
            users.is_active = is_active
            users.is_admin = is_admin

            res.send(users)
        } else {
            res.status(404).send({"error": "Usuário não existe!"})
        }
    } catch(err) {
        console.error(err.message)
        res.status(500).send({"error" : "Server Error"})
    }
})

router.patch('/:userId', (req, res, next)=> {
    try {
        let users = usersList.filter(u => u.id == req.params["userId"])
        if (users.length > 0){
            users = users[0]
            let { name, email, password, is_active, is_admin } = req.body
            for (const [chave, valor] of Object.entries(req.body)){
               if (chave == 'email' && (valor == "" || valor == null)){
                continue
               }
                users[chave] = valor
            }
            res.send(users)
        } else{
            res.status(404).send({"error": "user does not exist"})
        }
    } catch(err) {
        console.error(err.message)
        res.status(500).send({"error" : "Server Error"})
    }
})




router.get('/', (req, res, next) => {
   try{
         res.send(usersList)
    } catch (err) {
            console.error(err.message)
             res.status(500).send({"error" : "Server Error"})
         }
        
})

router.post('/', [], (req, res, next)=> {
    try {
        let { name, email, password, is_active, is_admin } = req.body
        let users = new User(name=name, email=email, password=password, is_active=is_active, is_admin=is_admin)
    
         /* if (!name || name === undefined || name === ''){
             res.status(400).send({"error" : "Insira um nome válido"})
         } */
         if (!email){
             res.status(400).send({"error" : "Insira um email válido"})
         }
         /* if (!password || password === undefined || password ===''){
             res.status(400).send({"error" : "Insira uma senha válida"})
         } */
         
         else{
            if(!(email.includes("@") && email.includes("."))){
                res.status(400).send({"error": "not a valid email"})
            }
    
            usersList.push(users)
            let body = req.body
            console.log(`${JSON.stringify(body)}`)
            
            res.send(usersList)
        }
    
       } catch (err) {
            console.error(err.message)
           res.status(500).send({"error" : "Server Error"})
       }
    })


/* router.post('/', [], (req, res, next)=> {
   try {
       let { name, email, password, is_active, is_admin } = req.body
       let users = new User(id=0, name=name, email=email, password=password, is_active=is_active, is_admin=is_admin)
   
        if (!name || name === undefined || name === ''){
            res.status(400).send({"error" : "Insira um nome válido"})
        }
        if (!email || email === undefined || email === ''){
            res.status(400).send({"error" : "Insira um email válido"})
        }
        if (!password || password === undefined || password ===''){
            res.status(400).send({"error" : "Insira uma senha válida"})
        } */
    // let users = {id: 0}
    // const {auth_name, auth_email, auth_password} = req.body
    // users.auth_name = auth_name
    // users.auth_email = auth_email
    // users.auth_password = auth_password
    
    // if (!auth_name || auth_name === undefined || auth_name === ''){
    //     res.status(400).send({"error" : "Insira o nome corretamente!"})
    // }
    // if (!auth_email || auth_email === undefined || auth_email === ''){
    //     res.status(400).send({"error" : "Insira o email corretamente!"})
    // }
    // if (!auth_password || auth_password === undefined || auth_password === ''){
    //     res.status(400).send({"error" : "Insira a senha corretamente!"})
    // }
    
   /*  else{
        if(!(email.includes("@") && email.includes("."))){
            res.status(400).send({"error": "not a valid email"})
        }

        users.id = usersList[usersList.length-1].id +1
        usersList.push(users)
        let body = req.body
        console.log(`${JSON.stringify(body)}`)
        
        res.send(usersList)
    }

   } catch (err) {
        console.error(err.message)
       res.status(500).send({"error" : "Server Error"})
   }
}) */

module.exports = router