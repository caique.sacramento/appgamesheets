const express = require('express')
const router = express.Router()

router.get('/', (req, res) => {

    res.send({"msg":"get jogo"})
    
})

router.post('/', (req, res)=> {
    try {
    const {nome, genero, produtora, data, classificacaoetaria, 
        quantidade, precodecompra, precodevenda} = req.body
    if (!nome || nome === undefined || nome === ''){
        res.status(400).send({"err" : "Insira o nome!"})
    }
    if (!genero || genero === undefined || genero === ''){
        res.status(400).send({"err" : "Insira o gênero!"})
    }
    if (!produtora || produtora === undefined || produtora === ''){
        res.status(400).send({"err" : "Insira a produtora!"})
    }
    if (!data || data === undefined || data === ''){
        res.status(400).send({"err" : "Insira a data de lançamento!"})
    }
    if (!classificacaoetaria || classificacaoetaria === undefined || classificacaoetaria === ''){
        res.status(400).send({"err" : "Insira a classificação etária!"})
    }
    if (!precodecompra || precodecompra === undefined || precodecompra === ''){
        res.status(400).send({"err" : "cadê o preço de compra, doidão?"})
    }
    if (!precodevenda || precodevenda === undefined || precodevenda === ''){
        res.status(400).send({"err" : "cadê o preço de venda, doidão?"})
    }
    else{
        // {console.log(`${nome}, ${genero}, ${produtora}, ${data}, ${classificacaoetaria}, 
        //  ${quantidade}, ${precodecompra}, ${precodevenda}`)}
        let body = req.body
        console.log(`${JSON.stringify(body)}`)
        // res.send(req.body)
        res.send(body)
    }
} catch (err) {
    res.status(500).send({"error" : "Server Error"})
}
})

module.exports = router