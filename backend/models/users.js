let usersList = []
class User {
    constructor(name, email, password, is_active=true, is_admin=false) {
        this.id = this.generate_id()
        this.name = name
        this.email = email
        this.password = password
        this.is_active = is_active
        this.is_admin = is_admin
    }
    generate_id() {
        if (usersList.length === 0){
            return 1
        }
        return usersList[usersList.length-1].id +1
    }
}

usersList.push(new User(name="Caique Murilo", email="caiquemurilo@sacramento.com", password="a12345", is_active=true, is_admin= true))
usersList.push(new User("Caique Mauricio", "caiquemauricio@sacramento.com", "a12345", is_active=false, is_admin=false))
usersList.push(new User(name="Angela Cristina", email="angelacristina@sacramento.com", password="a12345", is_active=false, is_admin= false))
usersList.push(new User(name="Manoel Maria", email="manoelmaria@sacramento.com", password="a12345", is_active=true, is_admin= true))
usersList.push(new User(name="Marcela Brandao", email="marcelabrandao@sacramento.com", password="a12345", is_active=true, is_admin= true))


module.exports = {
    usersList ,
    User
}
