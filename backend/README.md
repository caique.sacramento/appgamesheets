# bootcamp-treinamento-backend

primeiro projeto mensal bootcamp fullstack infnet
Api de CRUD de usuários

## Version

0.1.0 (beta)

## Prerequisites
*   node

## Installation

``` npm install ```

<!-- abaixo (ao lado do /api/), disponibilizar outras versões do API -->
## Root API

/api/ 

## TODO

* api POST user

* api GET user

* api GET user/:id

* api PUT user

* api PATCH user

* api DELETE user

* config MONGODB
